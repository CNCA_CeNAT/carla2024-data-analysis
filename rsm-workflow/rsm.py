# Data wrangling 
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA

# Visualization
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.express as px
import altair as alt
plt.rcParams['text.usetex'] = True

# Data analytics
import torch
import gpytorch
from gpytorch.models import ApproximateGP
from gpytorch.variational import CholeskyVariationalDistribution
from gpytorch.variational import VariationalStrategy

# -------------------------------- Gaussian Processes ----------------------------------- 

class GPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood):
        super(GPModel, self).__init__(train_x, train_y, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())
    
    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

class ApproxGPModel(ApproximateGP):
    def __init__(self, inducing_points):
        variational_distribution = CholeskyVariationalDistribution(inducing_points.size(0))
        variational_strategy = VariationalStrategy(self, inducing_points, variational_distribution, learn_inducing_locations=True)
        super(ApproxGPModel, self).__init__(variational_strategy)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

# -------------------------------- Gaussian Processes ----------------------------------- 
# ----------------------------------- RSM Analysis -------------------------------------- 

class RSM_Analysis():
    def __init__(self, filename, sheet):

        xlsx = pd.ExcelFile(filename)
        self.df = pd.read_excel(xlsx, sheet)
        self.df = self.df.set_index("Partition")

        # Put everything in a per second unit

        self.df["SPADES"] = 1/self.df["SPADES"]
        self.df["OSU_Latency"] = 1/self.df["OSU_Latency"]

        """with pd.option_context('display.max_rows', None, 'display.max_columns', None):  
            # more options can be specified also
    
            print(self.df.iloc[:,6:].groupby(self.df.index).min())
            print(self.df.iloc[:,6:].groupby(self.df.index).max())
            print(self.df.iloc[:,6:].groupby(self.df.index).median())

            print(self.df.iloc[:,:6].groupby(self.df.index).min())
            print(self.df.iloc[:,:6].groupby(self.df.index).max())
            print(self.df.iloc[:,:6].groupby(self.df.index).median())
        """
        self.df = self.filter_outliers(self.df, whisker_width=1.5)


        self.norm_df = self.df
        self.norm_df = (self.df - self.df.min())/(self.df.max() - self.df.min())
        self.norm_df = self.norm_df.fillna(self.norm_df.median())

        self.norm_xdf = self.norm_df.iloc[:,6:]
        self.norm_ydf = self.norm_df.iloc[:,:6]        

    @staticmethod
    def filter_outliers(df, whisker_width=1.5):
        """Remove outliers from a dataframe using the Median Absolute Deviation including optional whiskers, removing rows for which the column value are 
        less than Q1-sigma*MAD or greater than Q3+sigma*MAD.
        Args:
            df (`:obj:pd.DataFrame`): A pandas dataframe to subset
            whisker_width (float): Optional, loosen the IQR filter by a
                                factor of `whisker_width` * IQR.
        Returns:
            (`:obj:pd.DataFrame`): Filtered dataframe
        """
        
        for partition in pd.unique(df.index):
            subdf = df.loc[partition]
            # Calculate MAD

            mad = (subdf-subdf.median()).abs().median()
            subfilter = subdf.ge(subdf.median() - whisker_width*mad) & subdf.le(subdf.median() + whisker_width*mad)

            subdf = subdf[subfilter].fillna(subdf[subfilter].median())

            df.loc[partition] = subdf
        
        return df

    def get_PCA_components(self, N="opt", plot=False):
        """Get Principal Components for node configuration x_i (Performance data)
            N = "opt": use number of components that explains at least 95% of the variance of the dataset or more.
            N = (int): use N components of the analysis to run.
        """
        
        self.pca_func = PCA()
        self.pca_func.fit(self.norm_xdf)

        if plot:
            fig, ax = plt.subplots(figsize=(7,5), dpi=120)
            
            ax.plot(np.arange(1,self.norm_xdf.shape[1]+1), np.cumsum(self.pca_func.explained_variance_ratio_), marker='.')
            ax.hlines(y = 0.95, xmin=1, xmax=13, colors="r")
            ax.grid(alpha=0.5)
            ax.set_xlabel("Number of Components")
            ax.set_ylabel("Explained variance")

            fig.savefig("cov.png")
            fig.savefig("cov.svg")

        if N == "opt":
            ind = np.where(np.cumsum(self.pca_func.explained_variance_ratio_) > 0.95)
            n_comps = np.arange(0, self.norm_xdf.shape[1])+1
            n_comps = n_comps[ind]
            self.opt_ncomps = n_comps[0]
        elif type(N) == int:
            self.opt_ncomps = N
        else:
            raise Exception("Invalid N value.")
        
        self.pca_func = PCA(n_components=self.opt_ncomps)
        self.pca_comps = self.pca_func.fit_transform(self.norm_xdf)

    def GP_training(self, var, iters, N_pca="opt"):
        """Function that performs Gaussian Process Regression using GPyTorch     
        We use variational Gaussian Processes since there is underlying uncertainty in our data.   
        """
        self.var = var
        self.get_PCA_components(N_pca, plot=True)

        self.train_x = torch.Tensor(self.pca_comps)
        self.train_y = torch.Tensor(self.norm_ydf[var].values)

        self.likelihood = gpytorch.likelihoods.GaussianLikelihood()
        inducing_points = torch.randn(15, self.opt_ncomps)
        self.model = ApproxGPModel(inducing_points=inducing_points)

        self.model.train()
        self.likelihood.train()
        
        optimizer = torch.optim.Adam([
            {'params': self.model.parameters()},
            {'params': self.likelihood.parameters()},
            ],
            lr=0.01
        )

        #scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.1)

        # Our loss object. We're using the VariationalELBO
        mll = gpytorch.mlls.VariationalELBO(self.likelihood, self.model, num_data=self.train_y.size(0))

        for i in range(iters):
            # Now do the rest of the training loop
            optimizer.zero_grad()
            output = self.model(self.train_x)
            loss = -mll(output, self.train_y)
            loss.backward()
            optimizer.step()
            if (i % 10 == 0):
                print('Iter %d/%d - Loss: %.3f   lengthscale: %.3f   lr: %.5f' % (
                    i + 1, iters, loss.item(),
                    self.model.covar_module.base_kernel.lengthscale.item(),
                    optimizer.param_groups[0]["lr"]
                ))
            
    def GP_infer(self):
        """Performs Gaussian Process Inference and optimization towards a maximum in user application performance given a space of Nopt Principal Components, also transforms the result back to the node configuration space  
        """
        self.model.eval()
        self.likelihood.eval()

        var = self.var
        # we start from the clusters centers and their corresponding performance average values

        cluster_centers_x = self.norm_xdf.groupby(self.norm_xdf.index).mean()
        cluster_centers_x_ = self.df.iloc[:,6:].groupby(self.df.iloc[:,6:].index).mean()

        cluster_centers_y = self.norm_ydf[var].groupby(self.norm_ydf[var].index).mean()
        max_partition = cluster_centers_y.idxmax() # select node configuration with highest performance value
        max_cluster = cluster_centers_x.loc[max_partition]
        print(cluster_centers_x_.loc[max_partition])

        pca_cluster = self.pca_func.transform(max_cluster.values.reshape(1,-1))
        gamma = 1
        pca_cluster = torch.Tensor(pca_cluster)
        pca_cluster.requires_grad = True
        with gpytorch.settings.fast_pred_var():
            observed_pred = self.likelihood(self.model(pca_cluster.reshape(1,-1)))
        
        observed_pred.mean.backward(torch.ones_like(observed_pred.mean))
        dx = pca_cluster.grad
        self.new_pca_point = pca_cluster + gamma*dx
        self.new_point = self.pca_func.inverse_transform(self.new_pca_point.detach().numpy())

        print(self.new_point.reshape(-1)*(self.df.iloc[:,6:].max() - self.df.iloc[:,6:].min()) + self.df.iloc[:,6:].min() - cluster_centers_x_.loc[max_partition])
        
        # we take the centers with largest performance value
    
    def plot_2d(self):

        self.model.eval()
        self.likelihood.eval()

        x = torch.linspace(
            self.pca_comps[:,0].min()+self.pca_comps[:,0].min()*0.1,
            self.pca_comps[:,0].max()+self.pca_comps[:,0].max()*0.1,
            100
        )
        
        y = torch.linspace(
            self.pca_comps[:,1].min()+self.pca_comps[:,1].min()*0.1,
            self.pca_comps[:,1].max()+self.pca_comps[:,1].max()*0.1,
            100
        )

        X = torch.meshgrid(x, y, indexing="ij")
        
        tensorstack = [X[i].ravel() for i in range(len(X))]
        
        X_points = torch.stack(tensorstack, dim=0)
        X_points.requires_grad = True

        with gpytorch.settings.fast_pred_var():
            observed_pred = self.likelihood(self.model(X_points.T))
            
        observed_pred.mean.backward(torch.ones_like(observed_pred.mean))
        dx = X_points.grad        

        with torch.no_grad():
            fig, ax = plt.subplots(figsize=(7,5), dpi=150)

            heatmap = ax.pcolormesh(X[0], X[1], observed_pred.mean.reshape(X[0].shape), cmap="jet")

            ax.set_xlabel("PC-0")
            ax.set_ylabel("PC-1")
            
            n = 5
            ax.quiver(X[0][::n,::n], X[1][::n,::n], dx[0].reshape(X[0].shape)[::n,::n], dx[1].reshape(X[0].shape)[::n,::n], pivot="mid", color="black", label="Surface gradient", edgecolor='white', linewidth = 0.5)

            ax.scatter(self.train_x[:,0], self.train_x[:,1], c=self.train_y, edgecolors="white", cmap="jet", label="Observations")

            cluster_centers = self.norm_xdf.groupby(self.norm_xdf.index).mean()
            cluster_names = pd.unique(cluster_centers.index)
            
            pca_centers = self.pca_func.transform(cluster_centers)
            print(pca_centers)
            print(cluster_names)
            
            for i in range(len(cluster_names)):
                ax.text(
                    pca_centers[i,0]+0.08, 
                    pca_centers[i,1]-0.15, 
                    cluster_names[i],
                    color="white",
                    ha="center",
                    bbox=dict(boxstyle="round",
                        ec=(0, 0, 0),
                        fc=(0.196, 0.431, 0.871)
                    )
                )

            cb = fig.colorbar(heatmap)
            cb.ax.set_ylabel(self.var+" performance - normalized")
            
            ax.legend(loc="upper center", bbox_to_anchor=(0.55,1.08), ncols=2)
            fig.savefig("heatmap_"+self.var+".svg")
            fig.savefig("heatmap_"+self.var+".png")

    def make_radar_plots(self):
        
        radar_df = self.df
        radar_medians = radar_df.groupby(["Partition"]).median()

        radar_medians_norm = (radar_medians - radar_df.mean())/radar_df.std()
        radar_medians_norm = radar_medians_norm.fillna(0.0)

        radar_xdf = radar_medians_norm.iloc[:,6:]
        radar_ydf = radar_medians_norm.iloc[:,:6]
        radar_ydf.pop("Graph500_SSSP")
        radar_ydf.pop("Graph500_BFS")
        
        x_categories = radar_xdf.columns.tolist()
        y_categories = radar_ydf.columns.tolist()

        x_categories.append(x_categories[0])
        y_categories.append(y_categories[0])

        fig_x = go.Figure()
        fig_y = go.Figure()

        for i in range(radar_xdf.shape[0]):
            partition = radar_xdf.iloc[i]
            partitionlist = partition.tolist()
            partitionlist.append(partitionlist[0])  
            fig_x.add_trace(go.Scatterpolar(
                r=partitionlist,
                theta=x_categories,
                name=partition.name
            ))

        for i in range(radar_ydf.shape[0]):
            partition = radar_ydf.iloc[i]
            partitionlist = partition.tolist()
            partitionlist.append(partitionlist[0])    
            fig_y.add_trace(go.Scatterpolar(
                r=partitionlist,
                theta=y_categories,
                name=partition.name
            ))

        fig_x.update_layout(
            polar=dict(
                radialaxis=dict(
                visible=True
                ),
            ),
            showlegend=True
            )
        
        fig_y.update_layout(
            polar=dict(
                radialaxis=dict(
                visible=True
                ),
            ),
            showlegend=True
            )
        
        fig_x.write_image("radar_performance.svg",width=670, height=600, scale=2.5)
        fig_x.write_image("radar_performance.png",width=640, height=600, scale=2.5)

        fig_y.write_image("radar_apps.svg",width=670, height=600, scale=2.5)
        fig_y.write_image("radar_apps.png",width=600, height=600, scale=2.5)


# ----------------------------------- RSM Analysis -------------------------------------- 

if __name__ == "__main__":
    kabre_analysis = RSM_Analysis("Benchmark results.xlsx", "ALL_DATA")
    
    vars = ["PIC", "ResNet50", "SPADES", "Graph500"]

    # Visualization Gaussian Process - forced 2 Principal Components
    #for var in vars:
    #    kabre_analysis.GP_training(var, 1250, N_pca=2)
    #    kabre_analysis.plot_2d()
    #kabre_analysis.make_radar_plots()

    # Inference for provisioning and decision makign - N optimum components as defined in PCA analysis function
    kabre_analysis.GP_training(vars[0], 1250, N_pca="opt")
    kabre_analysis.GP_infer()

