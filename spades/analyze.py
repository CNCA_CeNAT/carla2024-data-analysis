import time
import datetime
directories = ['dribe', 'nu', 'nukwa', 'kura']
postfix = '_out'
configuration = ['K55']

def get_seconds(timestr):
    length = datetime.datetime.strptime(timestr, "%H:%M:%S.%f")
    length = datetime.timedelta(hours=length.hour, minutes=length.minute, seconds=length.second, milliseconds=length.microsecond//1000).total_seconds()
    return length

with open('out.csv', 'w') as o:
    o.write('Partition,Result,Configuration\n')
    for directory in directories:
        for i in range(1, 11):
            filename = f"{directory}/{i}{postfix}"
            length = 0
            try:
                with open(filename, 'r') as f:
                    for line in f:
                        if 'All done.' in line:
                            length += get_seconds(line.split()[0].strip())
                        elif 'Assembling time' in line:
                            length += get_seconds(line.split()[0].strip())
                o.write(f"{directory},{length},K55\n")
            except FileNotFoundError:
                o.write(f"{directory},{length},K55\n")
                pass