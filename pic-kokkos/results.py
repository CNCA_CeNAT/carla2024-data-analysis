from sys import argv

class Result:
    def __init__(self, partition, rate, initialization):
        self.partition = partition
        self.rate = rate
        self.initialization = initialization
    
    def __repr__(self):
        return f"{self.partition},{self.rate},{self.initialization}"
    
    def __str__(self):
        return f"{self.partition},{self.rate},{self.initialization}"

files = [name for name in argv[1:]]
results = []

for file in files:
    with open(file, 'r', encoding='utf8') as f:
        first_line = f.readline()
        partition = first_line.split(' ')[-1].split('-')[0]
        initialization = ''
        rate = -1
        for line in f:
            if "Initialization" in line:
                initialization = line.split(' ')[-1].strip()
            if "Rate" in line:
                rate = float(line.split(' ')[-1])
                results.append(Result(partition, rate, initialization))


with open('results.csv', 'w') as f:
    f.write('partition,rate,initialization\n')
    for result in results:
        f.write(f'{result}\n')