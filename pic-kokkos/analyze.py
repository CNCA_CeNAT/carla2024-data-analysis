# %%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
tex_fonts = {
    # Use LaTeX to write all text
    "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 10,
    "font.size": 10,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 8,
    "xtick.labelsize": 8,
    "ytick.labelsize": 8
}

plt.rcParams.update(tex_fonts)
# %%
results_df = pd.read_csv('results.csv')

# %%
g = sns.catplot(data=results_df, kind='bar', x='partition', y='rate', hue='initialization', errorbar='sd')
g.despine(left=True)
g.set_axis_labels(x_var='Kabré partition', y_var='Millions of particles moved per second')
g.savefig('results.png')
g.savefig('results.pdf')
g.savefig('results.svg')

# %%
