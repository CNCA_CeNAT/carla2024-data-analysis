# %%
import pandas as pd
import altair as alt
import numpy as np

# %%
def prep_df(df, name):
    df = df.stack().reset_index()
    df.columns = ['c1', 'c2', 'values']
    df['Group type'] = name
    return df
regions_df = pd.read_csv("data.csv", usecols=["Type", "Region", "Group Type"]).set_index('Region')

regions_df = pd.DataFrame(regions_df.groupby(by=["Region", "Group Type", "Type"])["Type"].count())
regions_df = pd.DataFrame(regions_df.rename(columns={"Type": "Count"}).to_records())
multicluster_df = regions_df[regions_df['Group Type'] == 'Multicluster'].drop('Group Type', axis=1)
single_df = regions_df[regions_df['Group Type'] == 'Single'].drop('Group Type', axis=1)
single_df.loc[len(single_df.index)] = (['Latin America', 'Supercomputer', 0])
single_df[(single_df["Region"] == "Europe") & (single_df["Type"] == "Cluster")]["Count"].values[0]

# %%
df1 = pd.DataFrame(index=single_df["Region"].unique(), columns=single_df["Type"].unique())
for idx in df1.index:
    for col in df1:
        df1.loc[idx, col] = single_df[(single_df["Region"] == idx) & (single_df["Type"] == col)]["Count"].values[0]
df2 = pd.DataFrame(index=multicluster_df["Region"].unique(), columns=multicluster_df["Type"].unique())
for idx in df2.index:
    for col in df2:
        df2.loc[idx, col] = single_df[(single_df["Region"] == idx) & (single_df["Type"] == col)]["Count"].values[0]
df2.insert(2, "Supercomputer", 0, True)

correspondence = {
    'Cluster': 'C',
    'Frankencluster': 'F',
    'Supercomputer': 'S'
}
df1 = prep_df(df1, "Single cluster").replace(correspondence)
df2 = prep_df(df2, "Multicluster").replace(correspondence)

# %%
df = pd.concat([df1, df2])
df
# %%
alt.Chart(df).mark_bar().encode(
    x=alt.X('c2:N', title=None),
    y=alt.Y('sum(values):Q', axis=alt.Axis(grid=False, title="Number of clusters")),
    column=alt.Column('c1:N', title=None),
    color=alt.Color('Group type:N', scale=alt.Scale(range=['#96ceb4', '#ffcc5c','#ff6f69'])),
    shape=alt.X('c2:N', title=None)
).configure_axisX(
    labelAngle=0
).configure_view(
    strokeOpacity=0    
).properties(
    width=120,
    height=80
).save('clusters_regions.pdf')
# %%
def get_specific_nodes(df, type):
    nodes = df[df['Type'] == type]['Number of Nodes'].values
    nodes.sort()
    nodes = nodes[nodes != 0]
    nodes = np.log2(nodes)
    nodes_p = np.linspace(0, 1, len(nodes))
    cluster_df = pd.DataFrame({'log2(Number of Nodes)': nodes, 'Probability': nodes_p, 'Type': type})
    return cluster_df
data = pd.read_csv("data.csv", usecols=["Type", "Number of Nodes"])
cluster_df = get_specific_nodes(data, 'Cluster')
franken_df = get_specific_nodes(data, 'Frankencluster')
# super_df = get_specific_nodes(data, 'Supercomputer')

complete_df = pd.concat([cluster_df, franken_df])
# complete_df = pd.concat([complete_df, super_df])
# %%
alt.Chart(complete_df).mark_point().encode(
    x='log2(Number of Nodes)',
    y='Probability',
    shape='Type',
    color='Type'
).properties(
    width=180,
    height=140
).save('nodes_scatter.pdf')
# %%
complete_df
# %%
